const grid = [];

const piecePositions = [
    {
        number: 3,
        origin: [3,3],
        rotation: 0,
        color: 'green'
    }
];

let cursorPiece = {
    number: 0,
    origin: [6,6],
    rotation: 0,
    color: 'green'
};

const canvas = document.querySelector('#canvas');
const ctx = canvas.getContext('2d');

const CWIDTH = canvas.clientWidth;
const CHEIGHT = canvas.clientHeight;

const SWIDTH = CWIDTH / 20;
const SHEIGHT = CHEIGHT / 20;

function resetGrid() {
    for (let x = 0; x < 20; x++) {
        grid[x] = [];
        for (let y = 0; y < 20; y++) {
            grid[x][y] = 'grey';
        }
    }
}

resetGrid();

function displayGrid() {
    for (const x in grid) {
        for (const y in grid[x]) {
            ctx.fillStyle = grid[x][y];
            ctx.fillRect(SWIDTH * x, SHEIGHT * y, SWIDTH, SHEIGHT);

            ctx.fillStyle = 'white';
            ctx.strokeRect(SWIDTH * x, SHEIGHT * y, SWIDTH, SHEIGHT);
        }
    }
}


function main() {
    displayGrid();

    paused = false;
    requestAnimationFrame(update);
}

let clickPosition = [];

let paused = false;
function update() {
    if (!paused) {

        ctx.clearRect(0,0,CWIDTH,CHEIGHT);

        if (clickPosition.length !== 0) {
            //grid[clickPosition[0]][clickPosition[1]] = 'green';
            const newOrigin = [clickPosition[0], clickPosition[1]];
            if (isPieceWithinGrid({...cursorPiece, origin: newOrigin})) {
                cursorPiece.origin = newOrigin;
            }
        }

        
        resetGrid();

        for (const i in piecePositions) {
            const piece = piecePositions[i];
            const pieceData = getPositionsOfPiece(piece.origin, piece.number, piece.rotation);

            for (const o of pieceData) {
                grid[o[0]][o[1]] = piece.color;
            }
        }

        const cursorPieceData = getPositionsOfPiece(cursorPiece.origin, cursorPiece.number, cursorPiece.rotation);
        for (const o of cursorPieceData) {
            grid[o[0]][o[1]] = cursorPiece.color;
        }
        
        displayGrid();
    }

    requestAnimationFrame(update);
}

function getPositionsOfPiece(origin, pieceNumber, rotation = 0) {
    const pieceData = PIECES[pieceNumber];
    const positions = [];
    for (const loc of pieceData) {
        const relX = loc[0];
        const relY = loc[1];

        // Flip?
        const flipX = rotation > 3 ? -relX : relX;


        // Rotation
        const rotX = Math.round(flipX * Math.cos(rotation * Math.PI / 2) + relY * Math.sin(rotation * Math.PI / 2));
        const rotY = Math.round(relY * Math.cos(rotation * Math.PI / 2) - flipX * Math.sin(rotation * Math.PI / 2));

        // Translation
        const newX = rotX + origin[0];
        const newY = rotY + origin[1];
        positions.push([newX, newY]);
    }
    return positions;
}

function isPieceWithinGrid(piece) {
    const pieceData = getPositionsOfPiece(piece.origin, piece.number, piece.rotation);
    let within = true;
    for (const location of pieceData) {
        if (grid[location[0]] === undefined || grid[location[0]][location[1]] === undefined) {
            within = false;
            break;
        }
    }
    console.log(within);

    return within;
}

function isPieceValid(piece) {
    let failedTests = 0;

    if (!isPieceWithinGrid(piece)) failedTests++;

    // TODO: write more tests

    // check if inside another piece

    // check if directly touching another piece

    return failedTests === 0;
}

ctx.canvas.onpointermove = (e) => {
    const rect = ctx.canvas.getBoundingClientRect();
    const gridX = Math.floor((e.clientX - rect.left) / (ctx.canvas.offsetWidth / 20));
    const gridY = Math.floor((e.clientY - rect.top) / (ctx.canvas.offsetHeight / 20));

    if (gridX >= 0 && gridX < 20 && gridY >= 0 && gridY < 20) {
        clickPosition = [gridX, gridY];
    }
}

ctx.canvas.onpointerdown = (e) => {
    placePiece(cursorPiece);
    cursorPiece.number = 21;
}

function placePiece(piece) {
    if (piece.number === 21) return;

    if (!isPieceValid(piece)) return;

    const copy = structuredClone(piece);
    piecePositions.push(copy);
}

onkeydown = (e) => {
    if (e.key === 'r' || e.key === 'ArrowRight') {
        const oldR = cursorPiece.rotation;

        cursorPiece.rotation++;
        if (cursorPiece.rotation % 4 === 0) {
            cursorPiece.rotation -= 4;
        }

        if (!isPieceWithinGrid(cursorPiece)) {
            cursorPiece.rotation = oldR;
        }
        
    } else if (e.key === 'ArrowLeft') {
        const oldR = cursorPiece.rotation;

        cursorPiece.rotation--;
        if (cursorPiece.rotation % 4 === 0) {
            cursorPiece.rotation += 4;
        }

        if (!isPieceWithinGrid(cursorPiece)) {
            cursorPiece.rotation = oldR;
        }
    }
    
    else if (e.key === 'f') {
        const oldR = cursorPiece.rotation;
        if (cursorPiece.rotation > 3) {
            cursorPiece.rotation -= 4;
        } else {
            cursorPiece.rotation += 4;
        }

        if (!isPieceWithinGrid(cursorPiece)) {
            cursorPiece.rotation = oldR;
        }
    } else if (e.key === 'p' || e.key === 'ArrowUp') {
        const oldNumber = cursorPiece.number;
        if (cursorPiece.number < 20) {
            cursorPiece.number++;
        } else {
            cursorPiece.number = 0;
        }

        if (!isPieceWithinGrid(cursorPiece)) {
            cursorPiece.number = oldNumber;
        }
    } else if (e.key === 'ArrowDown') {
        const oldNumber = cursorPiece.number;
        if (cursorPiece.number > 0) {
            cursorPiece.number--;
        } else {
            cursorPiece.number = 20;
        }

        if (!isPieceWithinGrid(cursorPiece)) {
            cursorPiece.number = oldNumber;
        }
    }
}

main();