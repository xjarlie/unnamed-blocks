const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const crypto = require('crypto');

const apiRouter = require('./webRouter');

const app = express();
const port = 3000;

app.set('view engine', 'ejs');

app.use('/public', express.static(path.join(__dirname, 'public')));
app.use(express.json());
app.use(cookieParser());

app.use((req, res, next) => {

    const playerID = req.cookies.playerID || crypto.randomUUID();
    res.cookie('playerID', playerID, { maxAge: 5184000000, httpOnly: true });

    req.playerID = playerID;

    next();

});

app.use('/', apiRouter);

app.get('/', (req, res) => {
    res.render('home');
});

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});