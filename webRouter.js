const express = require('express');
const router = express.Router();
const db = require('./conn');

router.get('/newgame', async (req, res) => {

    const playerID = req.playerID;

    // Generate unique game code
    let gameCode;
    let codeValid = false;
    while (codeValid === false) {
        codeValid = true;
        const number = Math.round(Math.random() * 65536);
        gameCode = number.toString(16).padStart(4, '0');
        const gameTest = await db.get(`/games/${gameCode}`);
        if (gameTest) {
            if (gameTest.expires < Date.now()) {
                codeValid = false;
            }
        }
    }

    const gameInfo = {
        code: gameCode,
        players: [
            playerID
        ],
        pieces: [],
        expires: Date.now() + 24*60*60*1000,
        state: 'open'
    };
    // state can be open,closed (TODO: add private invitation links), and inprogress (game cannot be joined)

    await db.set(`/games/${gameCode}`, gameInfo);

    res.redirect(`/game/${gameCode}`);

});

router.get('/game/:gameCode', async (req, res) => {

    const playerID = req.playerID;

    const game = await db.get(`/games/${req.params.gameCode}`);

    if (!game) { // game does not exist
        res.send('Game does not exist')
        return;
    }

    const { players, state } = game;

    

    if (!players.includes(playerID)) { // Player is not in game
        if (state === 'open') { // Game has not yet begun, and is open for anyone to join
            //res.redirect(`/join/${req.params.gameCode}`);

            players.push(playerID);

            await db.set(`/games/${req.params.gameCode}/players`, players);

        } else {
            res.send('Game is not open to join');
            return;
        }
    }

    res.render('game');
});

router.get(`/join/:gameCode`, async (req, res) => {

    const game = await db.get(`/games/${req.params.gameCode}`);

    res.send('join');
})

module.exports = router;