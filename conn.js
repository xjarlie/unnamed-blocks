const path = require('path');
const Database = require('x-jsondb');

const db = new Database(path.join(__dirname, 'db', 'db.json'));

module.exports = db;